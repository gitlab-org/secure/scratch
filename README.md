### Scratch pad project

This project is to be used as a "scratch pad" while developing security features that need ~"GitLab Ultimate" or ~"Enterprise Edition" features.

Please create a branch for testing your feature with the format `<gitlab-issue-id>-some-feature`.
