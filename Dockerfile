FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .

# build the sbomgen binary and automatically set the Version
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    go version && \
    go build -ldflags="-X 'main.Version=$CHANGELOG_VERSION'" -o /analyzer \
    main.go

FROM node:14-alpine3.12

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer"]
