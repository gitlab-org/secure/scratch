package main

import "fmt"

var (
	// Version is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	Version = "not-configured"
)

func main() {
	fmt.Printf("analyzer version %s\n", Version)
}
